import random
import sys

tries = 5


def words():
    # Function takes random word from list of the words
 word = random.choice(
    ["Warszawa",
              "Lubin",
              "Krakow",
              "Chorzow",
              "Poznan",
              "Kielce",
              "Wroclaw",
              "Warszawa",
              "Gdansk"
    ]
    ).lower()
 return word


def hangman_display(tries):
    # Function assign number of chances to each position of hangman levels
    # Picture of hangman levels shown in terminal
    hangman_levels = ['''
        +---+
        |
        |
        |
       ===''','''
        +---+
        |   O
        |
        |
       ===''','''
        +---+
        |   O
        |   |
        |
       ===''','''
        +---+
        |   O
        |  /|\ 
        |
       ===''','''
        +---+
        |   O
        |  /|\ 
        |  /
       ===''','''
        +---+
        |   O
        |  /|\ DEAD
        |  / \ 
       ==='''
]

    if tries ==5:
        print(hangman_levels[0])
    elif tries ==4: 
        print(hangman_levels[1])
    elif tries ==3: 
        print(hangman_levels[2])
    elif tries ==2: 
        print(hangman_levels[3])
    elif tries ==1: 
        print(hangman_levels[4])
    elif tries < 1: 
        print(hangman_levels[5])
    else:
        print("Something went wrong")

    return ""


def again():
    # Play again question
    again=str(input("Do you want to play again, type yes or no "))

    if again.lower() == "yes":
        game(tries)
    else:
        sys.exit(0)


def game(tries):
    # Start the game function
    word = words()
    guessed = []                                # Keeps the guessed letters
    wrong = []                                  # Keeps the already given letters
    print(hangman_display(tries))

    while tries > 0:
        # Shows the display of words in terminal ex. _ _ _ _ _ _ 
        out = ""
        for letter in word:
            if letter in guessed:
                out = out + letter
            else:
                out = out + "*"
        if out == word:
            break
       
        print("Guess the word:", out)
        print(f"You have {tries} chances left")
        guess = input().lower()                 # Letters input
        
        if guess in guessed or guess in wrong:  # Select letters and give them right place in right list 
            print("Already guessed", guess)     # ALready guessed
            hangman_display(tries)

        elif guess in word:                     # Correct letters in word 
            print("Great!")
            guessed.append(guess)
            hangman_display(tries)

        else:                                   # Bad letters and less chances 
            print("Nope") 
            tries = tries - 1
            hangman_display(tries)
            wrong.append(guess)
        print("Already given letters: ",  *wrong)

    if tries:                                   # Good answer
        print("You guessed", word.upper())
        again()
    else:                                       # Bad answer
        print("You didn't get", word.upper())
        again()


word = words()                                  # Call random word
game(tries)                                     # Call game 

# Paweu